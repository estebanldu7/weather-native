import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, View, TouchableWithoutFeedback, Animated, Alert} from 'react-native';
import {Picker} from '@react-native-community/picker';

const Form = ({search, setSearch, setGetApi}) => {

    const [buttonAnimation] = useState(new Animated.Value(1));
    const {city, country} = search;

    const inAnimation = () => {
        Animated.spring(buttonAnimation, {
            toValue: .9,
            useNativeDriver: true,
        }).start();
    };

    const outAnimation = () => {
        Animated.spring(buttonAnimation, {
            toValue: 1,
            friction: 4,
            tension: 30,
            useNativeDriver: true,
        }).start();
    };

    const animationStyle = {
        transform: [{scale: buttonAnimation}],
    };

    const findWeather = () => {
        if (country.trim() === '' || city.trim() === '') {
            showAlert();
            return;
        }

        setGetApi(true);
    };

    const showAlert = () => {
        Alert.alert(
            'Error',
            'Agrega una ciudad y selecciona un país para la busqueda',
            [{text: 'Entendido'}],
        );
    };

    return (
        <>
            <View>
                <View>
                    <TextInput
                        value={city}
                        style={styles.input}
                        onChangeText={city => setSearch({...search, city})}
                        placeholder="Ciudad"
                        placeholderTextColor="#666"
                    />
                </View>
                <View>
                    <Picker
                        selectedValue={country}
                        itemStyle={{height: 120, backgroundColor: '#FFF'}}
                        onValueChange={country => setSearch({...search, country})}
                    >
                        <Picker.Item label="-- Seleccione un pais -- " value=""/>
                        <Picker.Item label="ECUADOR" value="EC"/>
                        <Picker.Item label="USA" value="US"/>
                        <Picker.Item label="MEXICO" value="MX"/>
                        <Picker.Item label="ARGENTINA" value="AR"/>
                        <Picker.Item label="COLOMBIA" value="CO"/>
                        <Picker.Item label="COSTA RICA" value="CR"/>
                        <Picker.Item label="ESPAÑA" value="ES"/>
                        <Picker.Item label="PERU" value="PE"/>
                    </Picker>
                </View>

                <TouchableWithoutFeedback
                    onPressIn={() => inAnimation()}
                    onPressOut={() => outAnimation()}
                    onPress={() => findWeather()}
                >
                    <Animated.View style={[styles.btnSearch, animationStyle]}>
                        <Text style={styles.textSearch}>Buscar Clima</Text>
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    input: {
        padding: 10,
        height: 50,
        backgroundColor: '#FFF',
        fontSize: 20,
        marginBottom: 20,
        textAlign: 'center',
    },
    btnSearch: {
        marginTop: 50,
        backgroundColor: '#000',
        padding: 10,
        justifyContent: 'center',
    },
    textSearch: {
        color: '#fff',
        textAlign: 'center',
        textTransform: 'uppercase',
        fontWeight: 'bold',
        fontSize: 18,
    },
});

export default Form;
