/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import {
    StyleSheet,
    View,
    TouchableWithoutFeedback,
    Keyboard,
    Alert,
} from 'react-native';
import Form from './components/form';
import Weather from './components/weather';

const App = () => {

    const [search, setSearch] = useState({city: '', country: ''});
    const [getApi, setGetApi] = useState(false);
    const [bgColor, setBgColor] = useState('rgb(71, 149, 212)');
    const [resultApi, setResultApi] = useState({});
    const {city, country} = search;

    const hideKeyboard = () => {
        Keyboard.dismiss();
    };

    const bgColorApp = {
        backgroundColor: bgColor,
    };

    useEffect(() => {
        const resultWeather = async () => {
            if (getApi) {
                const apiKey = 'a83b5ffd03fcc7a22ac7714505962efa';
                const url = `http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${apiKey}`;

                try {
                    const response = await fetch(url);
                    const result = await response.json();
                    setResultApi(result);
                    setGetApi(false);

                    const kelvin = 273.15;
                    const {main} = resultApi;
                    let current = main.temp - kelvin;

                    if (current < 10) {
                        setBgColor('rgb(105, 108, 149)');
                    } else if (current >= 10 && current < 25) {
                        setBgColor('rgb(71, 149, 212)');
                    } else {
                        setBgColor('rgb(178, 28, 61)');
                    }
                } catch (error) {
                    showAlert();
                }
            }
        };

        resultWeather();

    }, [getApi]);

    const showAlert = () => {
        Alert.alert(
            'Error',
            'No existen resultados para tu busqueda',
            [{text: 'Entendido'}],
        );
    };

    return (
        <>
            <TouchableWithoutFeedback onPress={() => hideKeyboard()}>
                <View style={[bgColorApp, styles.app]}>
                    <View style={styles.content}>
                        <Weather result={resultApi}/>
                        <Form
                            search={search}
                            setSearch={setSearch}
                            setGetApi={setGetApi}
                        />
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </>
    );
};

const styles = StyleSheet.create({
    app: {
        flex: 1,
        justifyContent: 'center',
    },
    content: {
        marginHorizontal: '4%',
    },
});

export default App;
